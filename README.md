# i18ntutorial
This code is used to learn internationalization and localization concepts.  
Most of the stuff is in junit tests that can be run standalone or in your favorite IDE.  
Please be sure to read the javadocs for explanation.  

# Build
If using Windows, the console defaults to the windows-1252 charset.  This causes an issue with the ResourceBundleTest.test8_IDN unit test.  It is coded to not fail under these circumstances.
1. mvn clean install

# TODO
1. ~~Test ```String1.toLower().equals(String2.toLower())```~~
2. ~~Test URL in different encodings (IDN)~~
3. ~~Add Japanese kanji examples~~
1. Add JavaFX/GUI example with text sizing
1. Look into Javascript
1. Look into JSON
2. ~~Clean up TextFileEncodingTest.java~~
1. ~~Google translate the resource bundles~~
