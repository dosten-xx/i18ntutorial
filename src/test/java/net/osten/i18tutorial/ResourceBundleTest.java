/**
 * Copyright Northrop Grumman.
 */
package net.osten.i18tutorial;

import org.apache.commons.io.FileUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.net.IDN;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.ChoiceFormat;
import java.text.Collator;
import java.text.DateFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

/**
 * Java Internationalization and Localization tutorial.
 * Test methods are independent but follow a logical learning path.
 * Note that this file must be encoded as ISO-8859-1. Eclipse might encode it as UTF-8 so it will have to be changed.
 * Also, it is advised to change your Eclipse run configuration to use UTF-8 (on the common tab).
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ResourceBundleTest
{
   // Resource bundle to load (found in resources: Messages[_lang][_country],properties)
   private static final String BUNDLE = "Messages";

   static public void inspectCharacter(int c)
   {
      System.out.println("=> Inspecting char " + c);
      System.out.println("digit=" + Character.digit(c, 10));
      System.out.println("directionality=" + Character.getDirectionality(c));
      System.out.println("name=" + Character.getName(c));
      System.out.println("type=" + Character.getType(c));
      System.out.println("is alphabetic=" + Character.isAlphabetic(c));
      System.out.println("is defined=" + Character.isDefined(c));
      System.out.println("is digit=" + Character.isDigit(c));
      System.out.println("is lower case=" + Character.isLowerCase(c));
      System.out.println("is upper case=" + Character.isUpperCase(c));
      System.out.println("is valid code point=" + Character.isValidCodePoint(c));
      System.out.println("is surrogate=" + Character.isSurrogate((char) c));
   }

   /**
    * Tests loading the default locale and bundle.
    * Will load based on your operating system settings (e.g. en_US).
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void test1_DefaultLocalization() throws Exception
   {
      System.out.println("\n======== Test1 - Basic Localization with default locale");
      System.out.println("Default charset=" + Charset.defaultCharset().displayName());      

      testBundle(null, null);
   }

   /**
    * This demonstrates basic resource bundle usage.
    * Tests loading some ISO-8859-1 bundles.
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void test2_Localization() throws Exception
   {
      System.out.println("\n======== Test2 - Basic Localization");

      // This will load Messages.properties since en_US is the default locale.
      testBundle("en", "US");

      // This will load Messages_fr_FR.properties.
      testBundle("fr", "FR");

      // This will load the French default bundle; notice the farewell message ends in a period.
      testBundle("fr", null);

      // This will load the French default bundle since there is no bundle for Canadian French.
      // Notice the farewell message ends in a period.
      testBundle("fr", "CA");

      // This will load the Chinese bundle with a supplemental character.
      // Notice how the messages not in the Chinese bundle are retrieved from the default bundle.
      // See also test7 for info on supplemental characters.
      testBundle("CH", null, null, true, false);
   }

   /**
    * Shows an example of Unicode characters in the resource bundle property files.
    * The German properties files have Unicode characters. See the code comments for issues and solutions.
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void test3_UTF8Localization() throws Exception
   {
      System.out.println("\n======== Test3 - Using resource bundles with different encodings");

      // shows mangled characters because by default ResourceBundle's are read in as ISO-8859-1
      // Messages_de_DE is encoded as ISO-8859-1 but contains UTF-8 characters
      testBundle("de", "DE");

      // There are 4 approaches to the issue of properties file encoding...

      // option 1) - load with UTF-8 decoder custom control to force loading the properties files as UTF-8
      testBundle("de", "DE", new UTF8ResourceBundleControl());

      // option 2) - use ISO-8859-1 characters in your properties file (assuming that matches the language)
      // Messages_de_ZH.properties is encoded as ISO-8859-1 and the characters in the farewell are ISO-8859-1
      testBundle("de", "ZH");

      // option 3) - use Unicode escaping in an ISO-8859-1 encoded file
      // Messages_de_US.properties is has characters escaped using Unicode escaping.
      testBundle("de", "US");

      // option 4) - manually convert the strings to UTF-8 (see at the end of the testBundle method)
      testBundle("de", "DE", null, false, true);

   }

   /**
    * Example on handling compound messages and plurals.
    *
    * @throws Exception if something goes wrong.
    */
   @Test
   public void test4_CompoundMessages() throws Exception
   {
      System.out.println("\n======== Test4 - Basic compound messages");

      ResourceBundle messages = testBundle("en", "US");

      // create the desired locale and init a MessageFormat
      Locale currentLocale = new Locale("en", "US");
      MessageFormat formatter = new MessageFormat("");
      formatter.setLocale(currentLocale);

      // apply the main pattern
      formatter.applyPattern(messages.getString("eureka"));

      // create a ChoiceFormat to handle plurals
      double[] shipLimits = {
               0,
               1,
               2 };
      String[] shipStrings = {
               messages.getString("noShips"),
               messages.getString("oneShip"),
               messages.getString("manyShips") };
      ChoiceFormat choiceForm = new ChoiceFormat(shipLimits, shipStrings);

      // apply the formats to the formatter, must come after applyPattern and will override the formats specified in the pattern
      // this has formats for all the possible arguments in the compound pattern; note that plain strings do not need a format and are null
      // Warning: the order is the true order of the arguments in the pattern, not the index number
      Format[] formats = {
               NumberFormat.getCurrencyInstance(),
               DateFormat.getTimeInstance(),
               DateFormat.getDateInstance(DateFormat.SHORT),
               choiceForm,
               null,
               NumberFormat.getIntegerInstance() };
      formatter.setFormats(formats);

      // supply the pattern arguments; note that the numShips is supplied to both the ChoiceFormat and the value in the possible compound part
      for (int numShips = -1; numShips < 4; numShips++) {
    	 // the order of messageArguments match the index numbers in the properties string (see the 'eureka' message in Messages.properties)
         Object[] messageArguments = {
                  Integer.valueOf(numShips),
                  messages.getString("planet"),
                  new Date(),
                  5534423.34,
                  Integer.valueOf(numShips) };

         String output = formatter.format(messageArguments);
         System.out.println("formatted compound message=" + output);
      }
   }

   /**
    * Example on handling localized compound messages and plurals using non-default localization.
    *
    * @throws Exception if something goes wrong.
    */
   @Test
   public void test5_LocalizedCompoundMessages() throws Exception
   {
      System.out.println("\n======== Test5 - Compound messages with locales");

      ResourceBundle messages = testBundle("de", "DE");

      // create the desired locale and init a MessageFormat
      Locale currentLocale = new Locale("de", "DE");
      MessageFormat formatter = new MessageFormat("");
      formatter.setLocale(currentLocale);

      // apply the main pattern
      formatter.applyPattern(messages.getString("eureka"));

      // create a ChoiceFormat to handle plurals
      double[] shipLimits = {
               0,
               1,
               2 };
      String[] shipStrings = {
               messages.getString("noShips"),
               messages.getString("oneShip"),
               messages.getString("manyShips") };
      ChoiceFormat choiceForm = new ChoiceFormat(shipLimits, shipStrings);

      // apply the formats to the formatter, must come after applyPattern
      // this has formats for all the possible arguments in the compound pattern; note that plain strings do not need a format and are null
      // Note that the locales must be set here to provide localization.
      // Warning: the order is the actual order of the arguments in the pattern, not the argument index number
      Format[] formats = {
               NumberFormat.getCurrencyInstance(currentLocale),
               DateFormat.getTimeInstance(DateFormat.SHORT, currentLocale),
               DateFormat.getDateInstance(DateFormat.SHORT, currentLocale),
               choiceForm,
               null,
               NumberFormat.getIntegerInstance(currentLocale) };
      formatter.setFormats(formats);

      // supply the pattern arguments; note that the numShips is supplied to both the ChoiceFormat and the value in the possible compound part
      for (int numShips = 0; numShips < 4; numShips++) {
         Object[] messageArguments = {
                  Integer.valueOf(numShips),
                  messages.getString("planet"),
                  new Date(),
                  5534423.34,
                  Integer.valueOf(numShips) };

         String output = formatter.format(messageArguments);
         System.out.println("formatted compound message=" + output);
      }
   }

   /**
    * Example of string comparison and sorting.
    * Cannot rely on simple String.compareTo(); use Collator.compare instead.
    */
   @Test
   public void test6_StringCompareAndSort()
   {
      System.out.println("\n======== Test6 - String comparisons and sorting");

      // this sorting uses the default locale (US-us)
      String[] wordsToSort = {
               "cote",
               "coté",
               "côte",
               "côté" };
      sortStrings(Collator.getInstance(), wordsToSort);
      System.out.println(Arrays.toString(wordsToSort));

      // however, French has a different sorting rule for the special characters
      sortStrings(Collator.getInstance(new Locale("fr", "FR")), wordsToSort);
      System.out.println(Arrays.toString(wordsToSort));
   }

   /**
    * Brief example of supplemental characters (i.e. code points above 0xFFFF).
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void test7_SupplementalCodePoints() throws Exception
   {
      System.out.println("\n======== Test7 - Supplemental code points");

      // Code point U+25A6F (154233) is a supplemental code point.
      // It maps to the surrogate pair: 0xD856 (55382) 0xDE6F (56953).
      int codepoint = 154233;
      System.out.printf("\tcode point %d is supplementary=%s%n", codepoint, Character.isSupplementaryCodePoint(codepoint));
      System.out.printf("\tchars 55382, 56953 is surrogate=%s%n", Character.isSurrogatePair((char) 55382, (char) 56943));
      System.out.printf("\tcode point %d name:%s%n", codepoint, Character.getName(codepoint));
      System.out.printf("\tcode point %d as string=%s%n%n", codepoint, new String(Character.toChars(codepoint)));
      // U+254A6F results in two char's (aka surrogate pair).
      // Keep in mind that each of these char's are non-sensical by themselves.
      System.out.printf("\tchars for code point %d=[%d, %d]%n", codepoint, (int) Character.toChars(codepoint)[0], (int) Character.toChars(codepoint)[1]);
      // Going in reverse by taking the 2 chars to determine the code point.
      System.out.printf("\tcode point for chars=%s\n", Character.toCodePoint((char) 55382, (char) 56943));
   }

   /**
    * Demonstrates Internationalized Domain Names (RFC 3490).
    * The UTF-8 URL is read from a file to avoid encoding issues with source code.
    *
    * @throws IOException if something goes wrong
    */
   @Test
   public void test8_IDN() throws IOException
   {
      System.out.println("\n======== Test8 - International Domain Names");

      File in = new File("./src/test/resources/idn_chinese.txt");
      String utfChineseURL = FileUtils.readFileToString(in);
      System.out.printf("\tChinese IDN=%s%n", utfChineseURL);
      // Notice the very different encoding IDN uses.
      try {
    	   System.out.printf("\tASCII chinese=%s%n", IDN.toASCII(utfChineseURL));
         System.out.printf("\tUTF chinese=%s%n", IDN.toUnicode(IDN.toASCII(utfChineseURL)));
      }
      catch (IllegalArgumentException e) {
    	  // this happens if the default encoding is not Unicode, else just warn the user
    	  if (Charset.defaultCharset().displayName().equals("UTF-8")) {
    		  fail("Should not get parser error when using UTF-8.  Default charset is " + Charset.defaultCharset().displayName());
    	  }
    	  else {
    		  System.out.println("Warning: IDN cannot be tested correctly if the console does not support Unicode. Default charset is " + Charset.defaultCharset().displayName());
    	  }
      }
   }

   /**
    * Sorts an array of strings with a given Collator.
    *
    * @param collator collator to use
    * @param words strings to sort.
    */
   private void sortStrings(Collator collator, String[] words)
   {
      String tmp;
      for (int i = 0; i < words.length; i++) {
         for (int j = i + 1; j < words.length; j++) {
            if (collator.compare(words[i], words[j]) > 0) {
               tmp = words[i];
               words[i] = words[j];
               words[j] = tmp;
            }
         }
      }
   }

   /**
    * Loads a resource bundle and display some information about it along with some formatted strings.
    *
    * @param lang language
    * @param country country
    * @return loaded resource bundle
    * @throws Exception if something goes wrong
    */
   private ResourceBundle testBundle(String lang, String country) throws Exception
   {
      return testBundle(lang, country, null, false, false);
   }

   /**
    * Loads a resource bundle and display some information about it along with some formatted strings.
    *
    * @param lang language
    * @param country country
    * @param control optional control for loading the bundle (can be null)
    * @return loaded resource bundle
    * @throws Exception if something goes wrong
    */
   private ResourceBundle testBundle(String lang, String country, Control control) throws Exception
   {
      return testBundle(lang, country, control, false, false);
   }

   /**
    * Loads a resource bundle and display some information about it along with some formatted strings.
    *
    * @param lang language
    * @param country country
    * @param control optional control for loading the bundle (can be null)
    * @param inspectChars if true, extra info will be dumped for each character
    * @param manualConvertToUTF8 if true, the 'farewell' string will be manually converted to UTF-8
    * @return loaded resource bundle
    * @throws Exception if something goes wrong
    */
   private ResourceBundle testBundle(String lang, String country, Control control, boolean inspectChars, boolean manualConvertToUTF8) throws Exception
   {
      // load the local
      Locale currentLocale = null;
      if (lang == null && country == null) {
         // System.out.println("loading default locale");
         currentLocale = Locale.getDefault();
      }
      else if (lang != null && country == null) {
         // System.out.println("loading locale with lang");
         currentLocale = new Locale(lang);
      }
      else {
         // System.out.println("loading locale with lang and country");
         currentLocale = new Locale(lang, country);
      }

      // load the resource bundle
      ResourceBundle messages = null;
      if (control != null) {
         System.out.printf("\n\tshowing messages for %s-%s with control %s%n", lang, country, control.getClass().getName());
         // have to clear the cache here because the bundle may already be loaded without the custom Control
         System.out.println("\tlocale=" + currentLocale);
         ResourceBundle.clearCache();
         messages = ResourceBundle.getBundle(BUNDLE, currentLocale, control);
      }
      else {
         System.out.printf("\n\tshowing messages for %s-%s%n", lang, country);
         System.out.println("\tlocale=" + currentLocale);
         ResourceBundle.clearCache();
         messages = ResourceBundle.getBundle(BUNDLE, currentLocale);
      }

      System.out.println("\tgreeting=" + messages.getString("greetings"));
      System.out.println("\tinquiry=" + messages.getString("inquiry"));
      System.out.println("\tfarewell=" + messages.getString("farewell"));

      if (inspectChars) {
         String farewell = messages.getString("farewell");
         farewell.chars().forEach(c -> inspectCharacter(c));
      }

      if (manualConvertToUTF8) {
         String farewell = messages.getString("farewell");
         // option - manually convert strings
         String farewellUTF8 = new String(farewell.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
         System.out.println("\tfarewell in UTF8=" + farewellUTF8);
      }

      return messages;
   }
}
