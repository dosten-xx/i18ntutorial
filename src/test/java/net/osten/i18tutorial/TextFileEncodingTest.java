/*
 *
 */
package net.osten.i18tutorial;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.junit.Test;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Locale;

/**
 * These tests show how file encoding can affect the reading of files.
 * Each of the text files has some weird characters encoded differently.
 * Look at the assertions to see how the bytes, characters and strings are
 * created when the files are read with different encodings.
 * Note that question marks in the output represent illegal chars for the
 * encoding or characters that can't be displayed.
 * Might have to update your Eclipse run configuration to use ISO-8859-1/CP1252
 * encoding to keep Eclipse from defaulting to UTF-8 encoding.
 */
public class TextFileEncodingTest {
   /**
    * This test inputs an ANSI/ASCII encoded file.
    * ansi-test.txt has two bytes (C3 and B8) which represents Unicode code point
    * U+00F8 in UTF-8 (LATIN SMALL LETTER O WITH STROKE).
    * However, neither byte maps to a valid ASCII character since ASCII uses only
    * 7-bits.
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void testANSIFileEncoding() throws Exception {
      System.out.println("\n====> Testing ANSI encoding");
      runTestFile("/ansi-text.txt", 2, 1, 2);
   }

   /**
    * This test shows all attributes of Unicode characters.
    */
   @Test
   public void testCreatingUTF8String() {
      System.out.println("\n====> Testing character creation");

      String s1 = "\u00F8";
      System.out.println("s1=" + s1);
      assertThat(s1.length(), is(1));
      ResourceBundleTest.inspectCharacter(s1.charAt(0));

      // composable characters
      s1 = "\u0041" + "\u030A";
      System.out.println("s1=" + s1);
      assertThat(s1.length(), is(2));
      assertThat(s1.codePointCount(0, s1.length()), is(2));
      s1.chars().forEach(c -> ResourceBundleTest.inspectCharacter(c));

      // multi-char characters
      s1 = "\u00DF";
      System.out.println("s1=" + s1);
      s1 = "\u6771";
      System.out.println("s1=" + s1);
      ResourceBundleTest.inspectCharacter(s1.charAt(0));
      char[] chars = Character.toChars(0x10400);
      System.out.println("chars has " + chars.length + " characters");
      s1 = new String(chars);
      System.out.println("s1=" + s1);
      assertThat(s1.length(), is(2));
      assertThat(s1.codePointCount(0, s1.length()), is(1)); // note that code point count is 1 but char (aka code unit)
                                                            // count is 2
      s1.chars().forEach(c -> ResourceBundleTest.inspectCharacter(c));
      System.out.println("code point->");
      ResourceBundleTest.inspectCharacter(Character.toCodePoint(chars[0], chars[1]));
   }

   /**
    * Show platform defaults.
    * Will get different results depending on operating system, configuration, etc.
    */
   @Test
   public void testDisplayDefaults() {
      System.out.println("\nDefault charset=" + Charset.defaultCharset());
   }

   /**
    * Demonstrates Hebrew and Arabic UTF-8 encoded files.
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void testHebrewAndArabic() throws Exception {
      System.out.println("\n====> Testing Unicode encoding for Hebrew");
      byte[] raw = runTestFile("/utf8-hebrew.txt", 18, 10, 18);
      String s = new String(raw, StandardCharsets.UTF_8);
      assertThat(s.length(), is(10));
      s.chars().forEach(c -> ResourceBundleTest.inspectCharacter(c));

      System.out.println("\n====> Testing Unicode encoding for Arabic");
      raw = runTestFile("/utf8-arabic.txt", 26, 14, 26);
      s = new String(raw, StandardCharsets.UTF_8);
      assertThat(s.length(), is(14));
      s.chars().forEach(c -> ResourceBundleTest.inspectCharacter(c));
   }

   /**
    * This test inputs an ISO-8859-1 encoded file.
    * It has two bytes which represents U+00D3.
    * However, the ISO-8859-1 mapping is to LATIN CAPITAL LETTER A WITH TILDE
    * (U+00C3) and SET TRANSMIT STATE (U+0093).
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void testISO88591FileEncoding() throws Exception {
      System.out.println("\n====> Testing ISO-8859-1 encoding");
      runTestFile("/iso8859-text.txt", 2, 1, 2);
   }

   /**
    * Demonstrates Japanese UTF-8 encoded files (both with and without BOM).
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void testJapanese() throws Exception {
      // Notice how the BOM is an extra character at the beginning of the string
      // (ZERO-WIDTH NO-BREAK SPACE).
      // Something to keep in mind if you are counting characters.
      System.out.println("\n====> Testing Unicode encoding for Japanese");
      byte[] raw = runTestFile("/utf8-japanese.txt", 24, 8, 24);
      String s = new String(raw, StandardCharsets.UTF_8);
      assertThat(s.length(), is(8));
      s.chars().forEach(c -> ResourceBundleTest.inspectCharacter(c));

      // When there is no BOM, there is no leading ZERO-WIDTH NO-BREAK SPACE
      // character.
      System.out.println("\n====> Testing Unicode encoding for Japanese (no BOM)");
      raw = runTestFile("/utf8noBOM-japanese.txt", 21, 7, 21);
      s = new String(raw, StandardCharsets.UTF_8);
      assertThat(s.length(), is(7));
      s.chars().forEach(c -> ResourceBundleTest.inspectCharacter(c));
   }

   /**
    * This test inputs a UTF-16 encoded file.
    * UTF-16 always has a BOM and it depends on the byte order.
    * Bytes 2 and 3 are 00 F8 which is LATIN SMALL LETTER O WITH STROKE (U+00F8) in
    * Unicode.
    * Note that is a different encoding than for UTF-8.
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void testUTF16withBOMFileEncoding() throws Exception {
      // read lines using UTF-16
      System.out.println("\n====> Testing UTF-16 BE encoding");
      String inputFile = "/utf16BE-text.txt";
      InputStream is = this.getClass().getResourceAsStream(inputFile);
      byte[] raw = IOUtils.toByteArray(is);
      IOUtils.closeQuietly(is);
      assertThat(raw.length, is(4));
      System.out.println("raw bytes [" + raw.length + "]=" + bytesToHex(raw));
      is = this.getClass().getResourceAsStream(inputFile);
      String s1 = IOUtils.toString(is, StandardCharsets.UTF_16);
      IOUtils.closeQuietly(is);
      assertThat(s1.length(), is(1));
      System.out.println("s1 as " + StandardCharsets.UTF_16 + "=" + s1);
      is = this.getClass().getResourceAsStream(inputFile);
      char[] c1 = IOUtils.toCharArray(is, StandardCharsets.UTF_16);
      IOUtils.closeQuietly(is);
      assertThat(c1.length, is(1));
      System.out.println("c1 as " + StandardCharsets.UTF_16 + "[" + c1.length + "]=" + Arrays.toString(c1));

      // read lines using UTF-16 - Java detects the BOM correctly for BE and LE
      System.out.println("\n====> Testing UTF-16 LE encoding");
      inputFile = "/utf16LE-text.txt";
      is = this.getClass().getResourceAsStream(inputFile);
      raw = IOUtils.toByteArray(is);
      IOUtils.closeQuietly(is);
      assertThat(raw.length, is(4));
      System.out.println("raw bytes [" + raw.length + "]=" + bytesToHex(raw));
      is = this.getClass().getResourceAsStream(inputFile);
      s1 = IOUtils.toString(is, StandardCharsets.UTF_16);
      IOUtils.closeQuietly(is);
      assertThat(s1.length(), is(1));
      System.out.println("s1 as " + StandardCharsets.UTF_16 + "=" + s1);
      is = this.getClass().getResourceAsStream(inputFile);
      c1 = IOUtils.toCharArray(is, StandardCharsets.UTF_16);
      IOUtils.closeQuietly(is);
      assertThat(c1.length, is(1));
      System.out.println("c1 as " + StandardCharsets.UTF_16 + "[" + c1.length + "]=" + Arrays.toString(c1));

      // read lines using UTF-8 which will result in invalid characters
      inputFile = "/utf16BE-text.txt";
      is = this.getClass().getResourceAsStream(inputFile);
      s1 = IOUtils.toString(is, StandardCharsets.UTF_8);
      IOUtils.closeQuietly(is);
      assertThat(s1.length(), is(4));
      System.out.println("s1 as " + StandardCharsets.UTF_8 + "=" + s1);
      is = this.getClass().getResourceAsStream(inputFile);
      c1 = IOUtils.toCharArray(is, StandardCharsets.UTF_8);
      IOUtils.closeQuietly(is);
      assertThat(c1.length, is(4));
      System.out.println("c1 as " + StandardCharsets.UTF_8 + " [" + c1.length + "]=" + Arrays.toString(c1));
   }

   /**
    * This test inputs a UTF-8 encoded file without the BOM.
    * It has two bytes which represents U+00D3.
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void testUTF8noBOMFileEncoding() throws Exception {
      System.out.println("\n====> Testing UTF-8 without BOM encoding");
      byte[] raw = runTestFile("/utf8noBOM-text.txt", 2, 1, 2);

      // Here is an example of the danger of incorrectly assuming the encoding.
      // If run on Windows, the default encoding is 1252, so if you blindly create a
      // string from the raw bytes,
      // it will create a 2 character string neither of which is U+00D3.
      String s = new String(raw);

      assertThat(s.length(), is(1));
      s.chars().forEach(c -> ResourceBundleTest.inspectCharacter(c));
   }

   /**
    * This test inputs a UTF-8 encoded file the 3-byte UTF-8 BOM (ef bb bf).
    * Bytes 4 and 5 are c3 b8 which is LATIN SMALL LETTER O WITH STROKE (U+00F8) in
    * Unicode.
    * Note how the BOM creates an extra invalid character when read in as UTF-8.
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void testUTF8withBOMFileEncoding() throws Exception {
      System.out.println("\n====> Testing UTF-8 with BOM encoding");
      byte[] raw = runTestFile("/utf8-text.txt", 5, 2, 5);

      // Here is an example of the danger of incorrectly assuming the encoding.
      // If run on Windows, the default encoding is 1252, so if you blindly create a
      // string from the raw bytes,
      // it will create a 5 character string (3 from the BOM and 2 from the Unicode
      // character).
      // Warning: Java is not consistent with dealing with the BOM. This will create 3
      // bogus characters but the Japanese
      // example creates the single BOM character.
      System.out.println("\nCreating String with default charset");
      String s = new String(raw);
      if (System.getProperty("os.name").toLowerCase().contains("windows")) {
         // will be 5 chars
         Locale.getDefault();
      }
      assertThat(s.length(), is(2));
      s.chars().forEach(c -> ResourceBundleTest.inspectCharacter(c));

      // By specifying UTF-8, the String is created with the correct leading BOM
      // character.
      System.out.println("\nCreating String with UTF-8 charset");
      s = new String(raw, StandardCharsets.UTF_8);
      assertThat(s.length(), is(2));
      s.chars().forEach(c -> ResourceBundleTest.inspectCharacter(c));

      // workaround 1 - Apache Commons IO
      // https://commons.apache.org/proper/commons-io/javadocs/api-2.4/
      System.out.println("\nLoading UTF-8 with Commons IO BOMInputStream");
      BOMInputStream is = new BOMInputStream(this.getClass().getResourceAsStream("/utf8-text.txt"));
      assertThat(is.hasBOM(), is(Boolean.TRUE));
      System.out.println("\tBOM detected=" + is.hasBOM() + " and it is " + is.getBOMCharsetName());
      String s1 = IOUtils.toString(is, StandardCharsets.UTF_8);
      // now that extra character is gone!
      assertThat(s1.length(), is(1));
      System.out.println("\ts1 as " + StandardCharsets.UTF_8 + "=" + s1);
      assertThat(s1.chars().count(), is(1L));
      s1.chars().forEach(c -> ResourceBundleTest.inspectCharacter(c));
      IOUtils.closeQuietly(is);

      // workaround 2 - manually discard from the raw bytes (note that the BOM's are
      // different for different Unicode encodings)
      System.out.println("\nManually discarding BOM bytes");
      if (raw[0] == (byte) 0xEF && raw[1] == (byte) 0xBB && raw[2] == (byte) 0xBF) {
         System.out.println("\tdetected UTF-8 BOM");
         // but you still have to specify the encoding otherwise default charset may
         // mangle the string
         s1 = new String(Arrays.copyOfRange(raw, 3, 6), StandardCharsets.UTF_8);
         System.out.println("\ts1 as " + StandardCharsets.UTF_8 + "=" + s1);
      } else {
         fail("Should have detected the BOM");
      }
   }

   /**
    * This test inputs an Windows 1252 encoded file.
    * It has a single byte which represents a LATIN CAPITAL LETTER O WITH ACUTE
    * (U+00D3).
    * This maps to a valid 1252 character but not a valid Unicode character which
    * must have at least 2 bytes.
    *
    * @throws Exception if something goes wrong
    */
   @Test
   public void testWindows1252FileEncoding() throws Exception {
      System.out.println("\n====> Testing Windows 1252 encoding");
      runTestFile("/windows1252-text.txt", 1, 1, 1);
   }

   private String bytesToHex(byte[] in) {
      final StringBuilder builder = new StringBuilder();
      for (byte b : in) {
         builder.append(String.format("%02x", b));
         builder.append(' ');
      }
      return builder.toString();
   }

   /**
    * Reads in a file resource using different standard java character sets.
    *
    * @param inputFile                   resource path
    * @param expectedRawLength           length of the file in bytes
    * @param expectedUnicodeStringLength UTF-8 length of a string read from the
    *                                    file
    * @param expectedNonunicodeLength    non-unicode length (i.e. single byte
    *                                    encoding) of a string read from the file
    * @return raw bytes
    * @throws Exception if something goes wrong
    */
   private byte[] runTestFile(String inputFile, int expectedRawLength, int expectedUnicodeStringLength,
         int expectedNonunicodeLength) throws Exception {
      InputStream is = null;

      // read raw bytes
      is = this.getClass().getResourceAsStream(inputFile);
      byte[] raw = IOUtils.toByteArray(is);
      System.out.println("\tread " + raw.length + " bytes; expecting " + expectedRawLength + " raw bytes, "
            + expectedUnicodeStringLength + " unicode chars and " + expectedNonunicodeLength + " non-unicode chars");
      IOUtils.closeQuietly(is);
      assertThat(raw.length, is(expectedRawLength));
      System.out.println("\traw bytes [" + raw.length + "]=" + bytesToHex(raw));

      // read lines using UTF-8
      is = this.getClass().getResourceAsStream(inputFile);
      String s1 = IOUtils.toString(is, StandardCharsets.UTF_8);
      IOUtils.closeQuietly(is);
      assertThat(s1.length(), is(expectedUnicodeStringLength));
      System.out.println("\ts1 as " + StandardCharsets.UTF_8 + "=" + s1);
      is = this.getClass().getResourceAsStream(inputFile);
      char[] c1 = IOUtils.toCharArray(is, StandardCharsets.UTF_8);
      IOUtils.closeQuietly(is);
      assertThat(c1.length, is(expectedUnicodeStringLength));
      System.out.println("\tc1 as " + StandardCharsets.UTF_8 + " [" + c1.length + "]=" + Arrays.toString(c1));

      // read lines using ISO-8859-1
      is = this.getClass().getResourceAsStream(inputFile);
      s1 = IOUtils.toString(is, StandardCharsets.ISO_8859_1);
      IOUtils.closeQuietly(is);
      assertThat(s1.length(), is(expectedNonunicodeLength));
      System.out.println("\ts1 as " + StandardCharsets.ISO_8859_1 + "=" + s1);
      is = this.getClass().getResourceAsStream(inputFile);
      c1 = IOUtils.toCharArray(is, StandardCharsets.ISO_8859_1);
      IOUtils.closeQuietly(is);
      assertThat(c1.length, is(expectedNonunicodeLength));
      System.out.println("\tc1 as " + StandardCharsets.ISO_8859_1 + " [" + c1.length + "]=" + Arrays.toString(c1));

      is = this.getClass().getResourceAsStream(inputFile);
      s1 = IOUtils.toString(is, StandardCharsets.US_ASCII);
      System.out.println("\ts1 as " + StandardCharsets.US_ASCII + "=" + s1);
      IOUtils.closeQuietly(is);
      assertThat(s1.length(), is(expectedNonunicodeLength));
      is = this.getClass().getResourceAsStream(inputFile);
      c1 = IOUtils.toCharArray(is, StandardCharsets.US_ASCII);
      assertThat(c1.length, is(expectedNonunicodeLength));
      System.out.println("\tc1 as " + StandardCharsets.US_ASCII + " [" + c1.length + "]=" + Arrays.toString(c1));

      return raw;
   }

   private String getOS() {
      return System.getProperty("os.name");
   }
}
